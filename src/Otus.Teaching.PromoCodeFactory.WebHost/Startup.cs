using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Npgsql;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;


namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        
        public Startup(IConfiguration configuration) =>
            Configuration = configuration;
        
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddMvcOptions(x=> 
                x.SuppressAsyncSuffixInActionNames = false);
            services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));
            services.AddScoped<IDbInitializer, EfDbInitializer>();

            //// PostgreSQL
            var connectionStrBuilder = new NpgsqlConnectionStringBuilder();

            bool isDevelopment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
            if(isDevelopment)
            {
                connectionStrBuilder.ConnectionString = Configuration.GetConnectionString("PostgreSQL");
                connectionStrBuilder.Username = Configuration["ConnectionCredentials:UserId"];
                connectionStrBuilder.Password = Configuration["ConnectionCredentials:Password"];
            }
            else
            {
                connectionStrBuilder.ConnectionString = Configuration.GetConnectionString("InDockerPostgreSQL");
                connectionStrBuilder.Username = Environment.GetEnvironmentVariable("POSTGRES_USER");
                connectionStrBuilder.Password = Environment.GetEnvironmentVariable("POSTGRES_PASSWORD");
            }


            services.AddDbContext<DataContext>(ctx =>
            {
                ctx.UseNpgsql(connectionStrBuilder.ConnectionString);
                //ctx.UseLazyLoadingProxies();
            });

            //services.AddDbContext<DataContext>(x =>
            //{
            //    x.UseSqlite("Filename=PromoCodeFactoryDb.sqlite");
            //    x.UseLazyLoadingProxies();
            //});

            services.AddOpenApiDocument(options =>
            {
                options.Title = "PromoCode Factory API Doc";
                options.Version = "1.0";
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IDbInitializer dbInitializer)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseOpenApi();
            app.UseSwaggerUi3(x =>
            {
                x.DocExpansion = "list";
            });
            
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            
            dbInitializer.InitializeDb();
        }
    }
}